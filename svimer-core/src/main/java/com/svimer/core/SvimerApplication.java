/**
 *  Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 *
 *  @Package: com.svimer.core 
 *  @ClassName: SvimerApplication 
 *  @Description: TODO
 *  @author: Svimer 
 *  @date: 2019年9月7日 下午11:31:03 
 */
package com.svimer.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * spring boot1.5配置security关闭http基本验证,只需要在application.properites中配置security.basic.enabled=false即可，但是spring boot 2.0+之后这样配置就不能生效了。
 * 如果想要关闭验证功能：
 *	1、简单粗暴方法：
 *       把 Security包从pom.xml中移出去
 *	2、科学一点的：
 *     	在Application启动类上（或者任意@Configure配置类上）增加如下注解：
 *     	@EnableAutoConfiguration(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
 *    或者
 *    	//配置不需要登陆验证	
 *	  @Configuration
 *    @EnableWebSecurity
 *    static class WebSecurityConfig extends WebSecurityConfigurerAdapter {
 *		
 *        @Override
 *       protected void configure(HttpSecurity http) throws Exception {
 *        	//配置不需要登陆验证
 *            http.authorizeRequests().anyRequest().permitAll().and().logout().permitAll();
 *        }
 *   }
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableAutoConfiguration(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
@EnableEurekaClient
public class SvimerApplication {
	public static void main(String[] args) {
		SpringApplication.run(SvimerApplication.class, args);
	}
//	//配置不需要登陆验证	
//	@Configuration
//    @EnableWebSecurity
//    static class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//		
//        @Override
//        protected void configure(HttpSecurity http) throws Exception {
//        	//配置不需要登陆验证
//            http.authorizeRequests().anyRequest().permitAll().and().logout().permitAll();
//        }
//    }
}
