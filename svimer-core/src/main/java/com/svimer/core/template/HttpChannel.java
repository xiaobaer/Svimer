/**
 *  Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 *
 *  @Package: com.svimer.core.template 
 *  @ClassName: HttpChannel 
 *  @Description: TODO
 *  @author: Svimer 
 *  @date: 2019年9月8日 下午10:55:32 
 */
package com.svimer.core.template;

import javax.servlet.http.HttpServletRequest;

import com.svimer.core.utils.IPUtil;
import com.svimer.core.utils.SvimerUtil;
import com.svimer.core.utils.UserAgentUtil;

/**
 * 
 *
 */
public class HttpChannel {

	public static String getLogParam(HttpServletRequest request) {
		StringBuffer logParam = new StringBuffer();
		logParam.append(UserAgentUtil.getUserAgentInfo(request));
		
		
		logParam.append("IP:"+IPUtil.getIP(request));
		logParam.append(SvimerUtil.getSingleVerticalLine());
		logParam.append("Channel:HTTP.WAP");
		return logParam.toString();
		
		
	}
}
