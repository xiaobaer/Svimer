/**
 *  Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 *
 *  @Package: com.svimer.core.template 
 *  @ClassName: ChannelTemplate 
 *  @Description: TODO
 *  @author: Svimer 
 *  @date: 2019年9月8日 下午4:32:20 
 */
package com.svimer.core.template;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.svimer.core.utils.IdWorker;
import com.svimer.core.utils.SvimerUtil;

/**
 * 
 *
 */
public  class ChannelTemplate{
	private static final Logger LOG = LoggerFactory.getLogger(ChannelTemplate.class);

	/**
	 * @param request 
	 * @return
	 */
	public static String getLogParam(HttpServletRequest request) {
		StringBuffer logParam = commonParam(request);
		
		// URL
				String url = request.getRequestURL().toString();
				LOG.warn("URL={}", url);
				// http--.do,H5--hl,client--cl,微信--wx
				if (url.endsWith(".do")) {
					logParam = logParam.append(HttpChannel.getLogParam(request));
					
				}
				if (url.endsWith(".hl")) {
					logParam = logParam.append(H5Channel.getLogParam(request));
				}
				if (url.endsWith(".cl")) {
					logParam = logParam.append(ClientChannel.getLogParam(request));
				}
				if (url.endsWith(".wx")) {
					logParam = logParam.append(WXChannel.getLogParam(request));
				}
		return logParam.toString();
	}
	
	private static StringBuffer commonParam(HttpServletRequest request){
		StringBuffer commonParam = new StringBuffer();
		//全局流水号   
		HttpSession session = request.getSession();
		String sessionId = session.getId();
		commonParam.append("SessionId:"+sessionId);
		commonParam.append(SvimerUtil.getSingleVerticalLine());
		
		Long globalId =  (Long) session.getAttribute("GlobalId");
		if(null==globalId){
			IdWorker id = new IdWorker(0,1);
			globalId = id.nextId();
			session.setAttribute("GlobalId", globalId);
		}
		
		commonParam.append("GlobalId:"+globalId);
		commonParam.append(SvimerUtil.getSingleVerticalLine());
		
		return commonParam;
	}
	
}
