/**
 *  Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 *
 *  @Package: com.svimer.core.action 
 *  @ClassName: ActionTemplate 
 *  @Description: TODO
 *  @author: Svimer 
 *  @date: 2019年9月19日 下午3:32:57 
 */
package com.svimer.core.action;

import com.svimer.core.model.Context;

/**
 * 
 *
 */
public interface ActionTemplate {

	public String execute(Context context);
}
