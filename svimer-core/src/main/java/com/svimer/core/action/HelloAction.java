package com.svimer.core.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.svimer.core.model.Context;

@RestController
public class HelloAction implements ActionTemplate{

	private static final Logger LOG = LoggerFactory.getLogger(HelloAction.class);
	
	// ${env.name} + world 
    //相当于引用了yml文件中的  env.name配置
    //如果没有获取到env.name 的值，就会默认读取 :后面的值，相当于一个默认值了
    @Value("${server.port}")
    private String bar;
	
	/**
	 * @RequestMapping(value={"/hello.do","/hello.hl","/hello.cl","/hello.wx"}),http--.do,H5--hl,client--cl,微信--wx
	 * 
	 * @return
	 */
	@RequestMapping(value={"/hello.do","/hello.hl","/hello.cl","/hello.wx"})
	public String execute(Context context) {  
		
		LOG.info("context = "+context.toString());
		return "Hello " + bar + "!";
    }

	
	
}
