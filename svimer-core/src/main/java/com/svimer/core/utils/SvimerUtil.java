/**
 *  Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 *
 *  @Package: com.svimer.core.utils 
 *  @ClassName: SvimerUtil 
 *  @Description: TODO
 *  @author: Svimer 
 *  @date: 2019年9月19日 下午5:55:15 
 */
package com.svimer.core.utils;

/**
 * 
 *
 */
public class SvimerUtil {

	/**
	 * 获取竖线分割字符串
	 * @return
	 */
	public static String getSingleVerticalLine(){
		return " | ";
	}
	public static void main(String[] args) {
		System.out.println(SvimerUtil.getSingleVerticalLine());
	}
}
