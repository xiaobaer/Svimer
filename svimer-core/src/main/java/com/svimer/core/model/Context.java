/**
 *  Copyright © 2019 eSunny Info. Tech Ltd. All rights reserved.
 *
 *  @Package: com.svimer.core.model 
 *  @ClassName: Context 
 *  @Description: TODO
 *  @author: Svimer 
 *  @date: 2019年9月11日 上午10:34:56 
 */
package com.svimer.core.model;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.alibaba.fastjson.JSON;

/**
 * 
 *
 */
public class Context implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4700018043892237912L;
	private Map<String,Object> date = new ConcurrentHashMap<String, Object>();

	/**
	 * @return the date
	 */
	public Object getDate(String key) {
		return date.get(key);
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String key ,Object value) {
		date.put(key, value);
	}
	
	public void setAllDate(Map<String, String[]> map){
		date.putAll(map);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		
		return JSON.toJSONString(date);
	}
	
	

}
