package com.svimer.core.token;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.svimer.core.token.CirculateNoTokenBucket;
import com.svimer.core.token.Token;

public class CirculateNoTokenBucketTest {
	public static void main(String[] args) throws InterruptedException {
		CirculateNoTokenBucket circulateNoTokenBucket = CirculateNoTokenBucket.newBuilder().tokenBucketSize(15).build();
		int size = 4;
		ExecutorService pool = Executors.newFixedThreadPool(size);
		while (true) {
			TimeUnit.MILLISECONDS.sleep(500);
			pool.execute(new Task(circulateNoTokenBucket));

		}

	}

	static class Task implements Runnable {

		private CirculateNoTokenBucket circulateNoTokenBucket;

		public Task(CirculateNoTokenBucket circulateNoTokenBucket) {
			this.circulateNoTokenBucket = circulateNoTokenBucket;
		}

		@Override
		public void run() {
			boolean tokens = circulateNoTokenBucket.getTokens(1);
			if (tokens) {
				try {
					long startTime = System.currentTimeMillis(); // 获取出来的是当前时间的毫秒值
					// 业务处理耗时
					Random r = new Random();
					TimeUnit.MILLISECONDS.sleep(r.nextInt(1000));
					long endTime = System.currentTimeMillis(); // 获取出来的是当前时间的毫秒值
					// 毫秒
					long threshold = 500l;
					long timeDifference = endTime - startTime;
					if (timeDifference > threshold) {
						Token token = new Token();
						token.setEndTime(endTime);
						token.setStartTime(startTime);
						token.setTimeDifference(timeDifference);
						token.setToken("abc");
						token.setUrl("action地址");
						circulateNoTokenBucket.addAbnormalToken(token);
						System.out.println(circulateNoTokenBucket.getTokenQueueRemainingCapacity());
						System.out.println(circulateNoTokenBucket.getAbnormalTokensSize());
						 
						System.out.println(circulateNoTokenBucket.toString());
					} else {
						circulateNoTokenBucket.addTokens(1);
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				System.out.println("token rejuect=========Execute ThreadName=" + Thread.currentThread().getName());
			}

		}

	}
}
