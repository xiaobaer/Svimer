# Svimer

#### 介绍
快速开发框架

#### 软件架构
在SpringBoot2.0上开发
svimer-core:提供核心服务
svimer-web:
svimer-eureka-server:Eureka服务注册与发现
svimer-config-server:Spring Cloud Config配置服务
svimer-consumer:svimer-consumer模块将通过Feign组件调用svimer-core模块的服务，并通过Hystrix实现服务调用失败时的服务熔断
svimer-zuul:zuul 1.x系列
svimer-gateway:Spring Cloud Finchley版本的gateway
#### 安装教程

1. 免安装
2. 启动顺序：
	svimer-eureka-server  端口：8761
	svimer-config-server  端口：8762
	svimer-core           端口：8763
	svimer-consumer       端口：8764
	svimer-web            
	svimer-zuul           端口：8765    
	svimer-gateway        端口：8766
3. xxxx

#### 使用说明

1. 通过Git拉取master分支到本地，即可开发
2. 使用jdk1.8
3. Eclipse

